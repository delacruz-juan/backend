# WDC028 - S32 - Express.js - API Development (Part 1)

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1FgvQ-Och2oAlLNy-n_kwuVL4Dz7K__qUOV_cI2CcKHM/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1FgvQ-Och2oAlLNy-n_kwuVL4Dz7K__qUOV_cI2CcKHM/edit#slide=id.gca1de815d2_0_0) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s32-s36) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/backend/s32-s36/Manual.js) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                                      | Link                                                         |
| ------------------------------------------ | ------------------------------------------------------------ |
| npm Documentation                          | [Link](https://docs.npmjs.com/)                              |
| npm init                                   | [Link](https://docs.npmjs.com/cli/v7/commands/npm-init)      |
| Express JS Documentation - Getting Started | [Link](https://expressjs.com/en/starter/installing.html)     |
| Express JS express Function                | [Link](https://expressjs.com/en/api.html#express)            |
| Express JS json Method                     | [Link](https://expressjs.com/en/api.html#express.json)       |
| Express JS urlencoded Method               | [Link](https://expressjs.com/en/api.html#express.urlencoded) |
| Mongoose Documentation                     | [Link](https://mongoosejs.com/docs/api.html)                 |
| Mongoose connect Method                    | [Link](https://mongoosejs.com/docs/connections.html)         |
| Cross-Origin Resource Sharing (CORS)       | [Link](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS) |
| npm CORS package                           | [Link](https://www.npmjs.com/package/cors)                   |
| Mongoose Schemas                           | [Link](https://mongoosejs.com/docs/guide.html)               |
| JavaScript Date object                     | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date) |
| MVC Framework                              | [Link](https://www.tutorialspoint.com/mvc_framework/mvc_framework_introduction.htm) |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[1 hr] - Creating an Express JS backend application
	2.[1 hr] - Project Dependencies
		- CORS
		- Mongoose
	3.[1 hr] - Creating Models
		- Course
		- User
	4.[1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

​	

[Back to top](#table-of-contents)