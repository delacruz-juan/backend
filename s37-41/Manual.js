/*
===========================================
S32 - Express.js - API Development (Part 1)
===========================================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1FgvQ-Och2oAlLNy-n_kwuVL4Dz7K__qUOV_cI2CcKHM/edit#slide=id.gc44b0f8b88_0_669
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s32-s36

Other References:
	npm Documentation
		https://docs.npmjs.com/
	npm init
		https://docs.npmjs.com/cli/v7/commands/npm-init
	Express JS Documentation - Getting Started
		https://expressjs.com/en/starter/installing.html
	Express JS express Function
		https://expressjs.com/en/api.html#express
	Express JS json Method
		https://expressjs.com/en/api.html#express.json
	Express JS urlencoded Method
		https://expressjs.com/en/api.html#express.urlencoded
	Mongoose Documentation
		https://mongoosejs.com/docs/api.html
	Mongoose connect Method
		https://mongoosejs.com/docs/connections.html
	Cross-Origin Resource Sharing (CORS)
		https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
	npm CORS package
		https://www.npmjs.com/package/cors
	Mongoose Schemas
		https://mongoosejs.com/docs/guide.html
	JavaScript Date object
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date
	MVC Framework
		https://www.tutorialspoint.com/mvc_framework/mvc_framework_introduction.htm
	
	Creating Git Projects:
			GitLab
				https://gitlab.com/projects/new#blank_project
			GitHub
				https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
==========
Discussion
========== 
*/

/*
1. Create an "s32-s36" folder and create an empty "index.js" file that will serve as the entry point for our application.
	Application > index.js
*/

		/*
		Important Note:
			- Creating this file first is important before triggering the command to create a Node JS application in the following step.
			- Doing this first will automatically set the index.js file in the "package.json" file that will be created when the Node JS application is created.
		*/

/*
2. Create a Node JS application.
	Application > Terminal
*/

		npm init --y

		/*
		Important Note:
			- Triggering the command above will create a "package.json" file that will contain the details about our Node JS application.
			- If the "index.js" file was not created beforehand, you may change the "main" property to "index.js" to ensure that it would be the correct file that will be used as the entry point for the Node JS application.
			- Upon creation of this file, make sure to check that the correct file is set to ensure no problems will be encountered when trying to run the application.
			- Refer to "references" section of this file to find the documentations for npm Documentation and npm init.
		*/

/*
3. Install the Express JS Framework and cors package to be used in creating a basic Express JS application.
	Application > Terminal
*/

		npm install express

		/*
		Important Note:
			- Triggering the command above will install the latest version of Express JS framework and the cors package that we'll be using to easily setup a backend application.
			- There may be instances when an older version of a package is needed for a project, one reason for this is that a latest version of an application may have code breaking bugs or a preferred version of a package would like to be used.
			- To install older versions of packages, the following syntax may be used:
				- npm install packageName@versionNumber
			- The "cors" package will allow our backend application to be available to our frontend application.
			- By default our backend's CORS setting will prevent any application outside of our Express JS app to process requests to it. Using the cors package will allow us to manipulate this and control what applications may use our app.
			- Refer to "references" section of this file to find the documentation for Express JS Documentation - Getting Started.
		*/

/*
4. Create a simple Express JS application.
	Application > index.js
*/

		const express = require("express");
		// Creates an "app" variable that stores the result of the "express" function that initializes our express application and allows us access to different methods that will make backend creation easy
		const app = express();

		app.use(express.json());
		app.use(express.urlencoded({extended:true}));

		// Will used the defined port number for the application whenever an environment variable is available OR will used port 4000 if none is defined
		// This syntax will allow flexibility when using the application locally or as a hosted application
		

		//if(require.main) would allow us to listen to the app directly if it is not imported to another module, it will run the app directly.
		//else, if it is needed to be imported, it will not run the app and instead export it to be used in another file.
		if(require.main === module){
			app.listen(process.env.PORT || 4000, () => {
			    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
			});
		}

		module.exports = app;
		/*
		Important Note:
			- Refer to "references" section of this file to find the documentations for Express JS express Function, Express JS json Method and Express JS urlencoded Method.
		*/

/*
5. Run the Express JS app.
	Application > Terminal
*/

		nodemon index.js

/*
6. Install the mongoose package.
	Application > Terminal
*/

		npm install mongoose

		/*
		Important Note:
			- Mongoose is a package that we will be using to connect and communicate with our MongoDB Atlas as well as gain access to methods that will make CRUD operations easier to do.
			- Refer to "references" section of this file to find the documentation for Mongoose Documentation.
		*/

/*
7. Connect our Express JS application to our MongoDB Atlas database.
	Application > index.js
*/

		const express = require("express");
		const mongoose = require("mongoose");
		
		const app = express();

		// Connect to our MongoDB database
		mongoose.connect("mongodb+srv://admin:admin123@cluster0.7iowx.mongodb.net/S32-S36?retryWrites=true&w=majority", {
				useNewUrlParser: true,
				useUnifiedTopology: true
			});
		// Prompts a message in the terminal once the connection is "open" and we are able to successfully connect to our database
		mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'));

		app.use(express.json());
		/*...*/

		/*
		Important Note:
			- Mongoose is a package that we will be using to connect and communicate with our MongoDB Atlas as well as gain access to methods that will make CRUD operations easier to do.
			- Refer to "references" section of this file to find the documentation for Mongoose connect Method.
		*/

/*
8. Install the cors package.
	Application > Terminal
*/

		npm install cors

		/*
		Important Note
			- By default our backend's CORS setting will prevent any application outside of our Express JS app to process requests to it. Using the cors package will allow us to manipulate this and control what applications may use our app.
			- Refer to "references" section of this file to find the documentations for Cross-Origin Resource Sharing (CORS) and npm CORS package.
		*/

/*
8. Implement cors in our backend application.
	Application > Terminal
*/

		/*...*/
		const mongoose = require("mongoose");
		// Allows our backend application to be available to our frontend application
		// Allows us to control the app's Cross Origin Resource Sharing settings
		const cors = require("cors");

		/*...*/
		mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'));

		// Allows all resources to access our backend application
		app.use(cors());
		app.use(express.json());

		/*
		Important Note:
			- Using the above syntax to implement CORS in our application is not best practice because it allows all other applications to access our backend app.
			- This is only used during development for ease of access.
			- The proper way of doing this will be taught during the CSP3 hosting session.
		*/

/*
9. Create a "model" folder and create a "Course.js" file to store the schema for our courses.
	Application > models > Course.js
*/
		
		const mongoose = require("mongoose");

		const courseSchema = new mongoose.Schema({
			name : {
				type : String,
				// Requires the data for this our fields/properties to be included when creating a record.
				// The "true" value defines if the field is required or not and the second element in the array is the message that will be printed out in our terminal when the data is not present
				required : [true, "Course is required"]
			},
			description : {
				type : String,
				required : [true, "Description is required"]
			},
			price : {
				type : Number,
				required : [true, "Price is required"]
			},
			isActive : {
				type : Boolean,
				default : true
			},
			createdOn : {
				type : Date,
				// The "new Date()" expression instantiates a new "date" that stores the current date and time whenever a course is created in our database
				default : new Date()
			},
			// The "enrollees" property/field will be an array of objects containing the user IDs and the date and time that the user enrolled to the course
			// We will be applying the concept of referencing data to establish a relationship between our courses and users 
			enrollees : [
				{
					userId : {
						type : String,
						required: [true, "UserId is required"]
					},
					enrolledOn : {
						type : Date,
						default : new Date() 
					}
				}
			]
		})

		module.exports = mongoose.model("Course", courseSchema);

		/*
		Important Note
			- Make sure to follow the naming convention for model files which should have the folowing:
				- The first letter of the file should be capitalized to indicate that we are accessing the model file.
				- Should use the singular form of the collection.
			- We will be following the Model-View-Controllers Framework in creating our backend and frontend application.
			- This allows us to follow a certain standard when developing our applications and separating files into their own folders will allow us to easily locate the necessary files we need in developing our app.
			- Refer to "references" section of this file to find the documentations for Mongoose Schemas, JavaScript Date object and MVC Framework.
		*/

/*
========
Activity
========
*/

/*
1. Create a "User.js" file to store the schema for our users.
	Application > models > User.js
*/

		const mongoose = require("mongoose");

		const userSchema = new mongoose.Schema({
			firstName : {
				type : String,
				required : [true, "First name is required"]
			},
			lastName : {
				type : String,
				required : [true, "Last name is required"]
			},
			email : {
				type : String,
				required : [true, "Email is required"]
			},
			password : {
				type : String,
				required : [true, "Password is required"]
			},
			isAdmin : {
				type : Boolean,
				default : false
			},
			mobileNo : {
				type : String, 
				required : [true, "Mobile No is required"]
			},
			// The "enrollments" property/field will be an array of objects containing the course IDs, the date and time that the user enrolled to the course and the status that indicates if the user is currently enrolled to a course
			enrollments : [
				{
					courseId : {
						type : String,
						required : [true, "Course ID is required"]
					},
					enrolledOn : {
						type : Date,
						default : new Date()
					},
					status : {
						type : String,
						default : "Enrolled"
					}
				}
			]
		})

		module.exports = mongoose.model("User", userSchema);

/*
===========================================
S33 - Express.js - API Development (Part 2)
===========================================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/15fsby3B4BTi0RlreQwd2kD_zMCDDK7MOjGNiZzyNFcg/edit#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s32-s36

Other References:
	Express JS Routing
		https://expressjs.com/en/guide/routing.html
	Mongoose Queries
		https://mongoosejs.com/docs/queries.html
	Mongoose find Method
		https://mongoosejs.com/docs/api.html#model_Model.find
	JavaScript then Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/then
	Mongoose Models
		https://mongoosejs.com/docs/models.html
	bcrypt Package
		https://www.npmjs.com/package/bcrypt
	JSON Web Tokens
		https://jwt.io/
	jsonwebtoken Package
		https://www.npmjs.com/package/jsonwebtoken
	JavaScript typeof Operator
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof
	Creating Git Projects:
			GitLab
				https://gitlab.com/projects/new#blank_project
			GitHub
				https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
==========
Discussion
========== 
*/

/*
1. Create a "routes" folder, create a "user.js" file to store the routes for our users and create a "checkEmail" route.
	Application > routes > user.js
*/

		const express = require("express");
		const router = express.Router();

		// Route for checking if the user's email already exists in the database
		// Invokes the checkEmailExists function from the controller file to communicate with our database
		// Passes the "body" property of our "request" object to the corresponding controller function
		router.post("/checkEmail", (req, res) => {
			userController.checkEmailExists(req.body);
		})

		// Allows us to export the "router" object that will be accessed in our "index.js" file
		module.exports = router;

		/*
		Important Note
			- All files pertaining to our users are named "user" to follow good practice and standards for naming our files.
			- This is confusing for beginners but may be packaged to them by the way the files are accessed. (e.g. "/models/User" refers to the user model and "/routes/user" refers to the user routes file)
			- You may change the name of the files (e.g. "userRoutes" for the route file and userController for the controller file) for the student's convenience but make sure to highlight the importance of using these naming conventions which might confuse them when they encounter it in real world application.
			- Using these naming conventions also makes it easier during development which shortens the syntax.
			- Refer to "references" section of this file to find the documentation for Express JS Routing.
		*/

/*
2. Implement the main route for our user routes.
	Application > index.js
*/

		/*...*/
		const cors = require("cors");
		// Allows access to routes defined within our application
		const userRoutes = require("./routes/user");

		const app = express();

		/*...*/

		app.use(express.urlencoded({extended:true}));

		// Defines the "/users" string to be included for all user routes defined in the "user" route file
		app.use("/users", userRoutes);

		if(require.main === module){
			app.listen(process.env.PORT || 4000, () => {
			    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
			});
		}

		module.exports = app;
/*
3. Create a "controllers" folder, create a "user.js" file to store the controller functions that will contain the business logic for our user CRUD operations.
	Application > controllers > user.js
*/

		// The "User" variable is defined using a capitalized letter to indicate that what we are using is the "User" model for code readability
		const User = require("../models/User");

		// Check if the email already exists
		/*
			Steps: 
			1. Use mongoose "find" method to find duplicate emails
			2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
		*/
		module.exports.checkEmailExists = (reqBody) => {

			// The result is sent back to the frontend via the "then" method found in the route file
			return User.find({email : reqBody.email}).then(result => {

				// The "find" method returns a record if a match is found
				if (result.length > 0) {

					return true;

				// No duplicate email found
				// The user is not yet registered in the database
				} else {

					return false;

				};
			});

		};

		/*
		Important Note
			- Refer to "references" section of this file to find the documentations for Mongoose Queries, Mongoose find Method and JavaScript then Method.
		*/

/*
4. Send the response back to our frontend application.
	Application > routes > user.js
*/

		/*...*/
		const router = express.Router();
		const userController = require("../controllers/user");

		// The full route to access this is "http://localhost:4000/users/checkEmail" where the "/users" was defined in our "index.js" file
		// The "then" method uses the result from the controller function and sends it back to the frontend application via the "res.send" method
		router.post("/checkEmail", (req, res) => {
			userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
		});

		/*...*/

		/*
		Important Note:
			- Highlight the process of creating models, routes and controllers when developing a backend application to reinforce the process of routes and controller creation.
		*/

/*
5. Process a POST request at the "/checkEmail" route using postman to check for duplicate emails.
	Postman
*/

		url: http://localhost:4000/users/checkEmail
		method: POST
		body: raw + JSON
			{
			    "email": "john@mail.com"
			}

/*
6. Create a route for registering a user.
	Application > routes > user.js
*/

		/*...*/

		router.post("/checkEmail", (req, res) => {
			/*...*/
		});

		// Route for user registration
		router.post("/register", (req, res) => {
			userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
		});

		module.exports = router;

/*
7. Create a controller method for registering a user.
	Application > controllers > user.js
*/

		/*...*/

		module.exports.checkEmailExists = (reqBody) => {
			/*...*/
		};

		// User registration
		/*
			Steps:
			1. Create a new User object using the mongoose model and the information from the request body
			2. Make sure that the password is encrypted
			3. Save the new User to the database
		*/
		module.exports.registerUser = (reqBody) => {

			// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
			// Uses the information from the request body to provide all the necessary information
			let newUser = new User({
				firstName : reqBody.firstName,
				lastName : reqBody.lastName,
				email : reqBody.email,
				mobileNo : reqBody.mobileNo,
				password : reqBody.password
			})

			// Saves the created object to our database
			return newUser.save().then((user, error) => {

				// User registration failed
				if (error) {

					return false;

				// User registration successful

				} else {

					return true;

				};

			});

		};

		/*
		Important Note
			- Refer to "references" section of this file to find the documentations for Mongoose Models.
		*/

/*
8. Process a POST request at the "/register" route using postman to create a user record.
	Postman
*/

		url: http://localhost:4000/users/register
		method: POST
		body: raw + JSON
			{
				"firstName": "John",
				"lastName": "Smith",
			    "email": "john@mail.com",
			    "mobileNo": "09123456789",
			    "password": "john1234"
			}

/*
9. Install the "bcrypt" package.
	Application > Terminal
*/

		npm install bcrypt

		/*
		Important Note
			- In our application we will be using the bcrypt package to demonstrate how to encrypt password data when registering a user.
			- The "bcrypt" package is one of the many packages that we can use to encrypt information but is not commonly recommended because of how simple the algorithm is for creating encrypted passwords which have been decoded by hackers.
			- There are other more advanced encryption packages that can be used.
			- This is used in the bootcamp to easily demonstrate how encryption works.
			- Refer to "references" section of this file to find the documentation for bcrypt Package.
		*/

/*
10. Implement "bcrypt" in our application and refactor the "register" controller method.
	Application > controllers > user.js
*/

		/*...*/

		module.exports.registerUser = (reqBody) => {

			let newUser = new User({
				/*...*/
				// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
				password : bcrypt.hashSync(body.password, 10)
			})

			/*...*/

		};

		/*
		Important Note:
			- Make sure to delete all user records in the database before registering for a new user to ensure that all user records found in it would have encrypted passwords.
		*/

/*
11. Install the "jsonwebtoken" package.
	Application > Terminal
*/

		npm install jsonwebtoken

		/*
		Important Note
			- JSON web tokens are an industry standard for sending information between our applications in a secure manner.
			- The "jsonwebtoken" package will allow us to gain access to methods that will help us create a JSON web token.
			- Refer to "references" section of this file to find the documentations for JSON Web Tokens and jsonwebtoken Package.
		*/

/*
12. Create an "auth.js" file to store the methods for creating jsonwebtokens.
	Application > auth.js
*/

		const jwt = require("jsonwebtoken");
		// User defined string data that will be used to create our JSON web tokens
		// Used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined secret keyword
		const secret = "CourseBookingAPI";

		// [Section] JSON Web Tokens
		/*
		- JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
		- Information is kept secure through the use of the secret code
		- Only the system that knows the secret code that can decode the encrypted information

		- Imagine JWT as a gift wrapping service that secures the gift with a lock
		- Only the person who knows the secret code can open the lock
		- And if the wrapper has been tampered with, JWT also recognizes this and disregards the gift
		- This ensures that the data is secure from the sender to the receiver
		*/

		// Token creation
		/*
		- Analogy
			Pack the gift and provide a lock with the secret code as the key
		*/
		module.exports.createAccessToken = (user) => {
			// The data will be received from the registration form
			// When the user logs in, a token will be created with user's information
			const data = {
				id : user._id,
				email : user.email,
				isAdmin : user.isAdmin
			};

			// Generate a JSON web token using the jwt's sign method
			// Generates the token using the form data and the secret code with no additional options provided
			return jwt.sign(data, secret, {});
			
		};

/*
13. Create a route for authenticating a user.
	Application > routes > user.js
*/

		/*...*/

		router.post("/register", (req, res) => {
			/*...*/
		});

		// Route for user authentication
		router.post("/login", (req, res) => {
			userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
		});

		/*...*/

/*
14. Create a controller method for authenticating a user.
	Application > controllers > user.js
*/

		/*...*/
		const bcrypt = require("bcrypt");
		const auth = require("../auth");

		module.exports.checkEmailExists = (reqBody) => {
			/*...*/
		};

		module.exports.registerUser = (reqBody) => {
			/*...*/
		};

		// User authentication
		/*
			Steps:
			1. Check the database if the user email exists
			2. Compare the password provided in the login form with the password stored in the database
			3. Generate/return a JSON web token if the user is successfully logged in and return false if not
		*/
		module.exports.loginUser = (reqBody) => {
			// The "findOne" method returns the first record in the collection that matches the search criteria
			// We use the "findOne" method instead of the "find" method which returns all records that match the search criteria
			return User.findOne({email : reqBody.email}).then(result => {

				// User does not exist
				if(result == null){

					return false;

				// User exists
				} else {

					// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
					// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
					// A good coding practice for boolean variable/constants is to use the word "is" or "are" at the beginning in the form of is+Noun
						//example. isSingle, isDone, isAdmin, areDone, etc..
					const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

					// If the passwords match/result of the above code is true
					if (isPasswordCorrect) {

						// Generate an access token
						// Uses the "createAccessToken" method defined in the "auth.js" file
						// Returning an object back to the frontend application is common practice to ensure information is properly labeled and real world examples normally return more complex information represented by objects
						return { access : auth.createAccessToken(result) }

					// Passwords do not match
					} else {

						return false;

					};

				};

			});

		};

/*
15. Process a POST request at the "/login" route using postman to authenticate a user.
	Postman
*/

		url: http://localhost:4000/users/login
		method: POST
		body: raw + JSON
			{
			    "email": "john@mail.com",
			    "password": "john1234"
			}

/*
========
Activity
========
*/

/*
1. Create a route for retrieving the details of a user.
	Application > routes > user.js
*/

		/*...*/

		router.post("/login", (req, res) => {
			/*...*/
		});

		// Route for retrieving user details
		router.post("/details", (req, res) => {

			// Provides the user's ID for the getProfile controller method
			userController.getProfile({userId : req.body.id}).then(resultFromController => res.send(resultFromController));

		});

		/*...*/

/*
2. Create a controller method for retrieving the details of the user.
	Application > controllers > user.js
*/

		/*...*/

		module.exports.loginUser = (reqBody) => {
			/*...*/
		}

		// Retrieve user details
		/*
			Steps:
			1. Find the document in the database using the user's ID
			2. Reassign the password of the returned document to an empty string
			3. Return the result back to the frontend
		*/
		module.exports.getProfile = (data) => {

			return User.findById(data.userId).then(result => {

				// Changes the value of the user's password to an empty string when returned to the frontend
				// Not doing so will expose the user's password which will also not be needed in other parts of our application
				// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
				result.password = "";

				// Returns the user information with the password as an empty string
				return result;

			});

		};

/*
3. Process a POST request at the "/details" route using postman to retrieve the details of the user.
	Postman
*/

		url: http://localhost:4000/users/details
		method: POST
		body: raw + JSON
			{
			    "id": "613e77c9423177a5ab9a066d"
			}

/*
===========================================
S34 - Express.js - API Development (Part 3)
===========================================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1IN24Fx5PnrlIq4Bd_UIYwQJg93xqisWRRnH0HuRTrm0/edit#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s32-s36

Other References:
	JSON Web Tokens
		https://jwt.io/
	jsonwebtoken Package
		https://www.npmjs.com/package/jsonwebtoken
	What Is Middleware
		https://www.redhat.com/en/topics/middleware/what-is-middleware
	JavaScript next Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Generator/next
	JavaScript typeof Operator
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof
	Creating Git Projects:
			GitLab
				https://gitlab.com/projects/new#blank_project
			GitHub
				https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
==========
Discussion
========== 
*/

/*
1. Create a "verify" method that will verify the validity of the JWT.
	Application > auth.js
*/

		/*...*/

		module.exports.createAccessToken = (user) => {
			/*...*/
		}

		// Token Verification
		/*
		- Analogy
			Receive the gift and open the lock to verify if the the sender is legitimate and the gift was not tampered with
		*/
		module.exports.verify = (req, res, next) => {

			// The token is retrieved from the request header
			// This can be provided in postman under
				// Authorization > Bearer Token
			let token = req.headers.authorization;

			// Token recieved and is not undefined
			if (typeof token !== "undefined") {

				console.log(token);

				// The "slice" method takes only the token from the information sent via the request header
				// The token sent is a type of "Bearer" token which when recieved contains the word "Bearer " as a prefix to the string
				// This removes the "Bearer " prefix and obtains only the token for verification
				token = token.slice(7, token.length);

				// Validate the token using the "verify" method decrypting the token using the secret code
				return jwt.verify(token, secret, (err, data) => {

					// If JWT is not valid
					if (err) {

						return res.send({auth : "failed"});

					// If JWT is valid
					} else {

						// Allows the application to proceed with the next middleware function/callback function in the route
						// The verify method will be used as a middleware in the route to verify the token before proceeding to the function that invokes the controller function
						next();

					}
				})

			// Token does not exist
			} else {

				return res.send({auth : "failed"});

			};

		};

		/*
		Important Note
			- Refer to "references" section of this file to find the documentations for JSON Web Tokens and jsonwebtoken Package.
		*/

/*
2. Create a "decode" method that will decrypt the JSON web token to obtain the information found in it.
	Application > auth.js
*/

		/*...*/

		module.exports.verify = (req, res, next) => {
			/*...*/
		}

		// Token decryption
		/*
		- Analogy
			Open the gift and get the content
		*/
		module.exports.decode = (token) => {

			// Token recieved and is not undefined
			if(typeof token !== "undefined"){

				// Retrieves only the token and removes the "Bearer " prefix
				token = token.slice(7, token.length);

				return jwt.verify(token, secret, (err, data) => {

					if (err) {

						return null;

					} else {

						// The "decode" method is used to obtain the information from the JWT
						// The "{complete:true}" option allows us to return additional information from the JWT token
						// Returns an object with access to the "payload" property which contains user information stored when the token was generated
						// The payload contains information provided in the "createAccessToken" method defined above (e.g. id, email and isAdmin)
						return jwt.decode(token, {complete:true}).payload;
					};

				})

			// Token does not exist
			} else {

				return null;

			};

		};

/*
3. Refactor the "/details" route to implement the "verify" method.
	Application > routes > user.js
*/

		/*...*/
		const userController = require("../controllers/user");
		const auth = require("../auth");

		router.post("/checkEmail", (req, res) => {
			/*...*/
		});

		/*...*/

		router.post("/login", (req, res) => {
			/*...*/
		});

		// The "auth.verity" acts as a middleware to ensure that the user is logged in before they can enroll to a course
		router.get("/details", auth.verify, (req, res) => {

			// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
			const userData = auth.decode(req.headers.authorization);

			userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

		});

		/*...*/

		/*
		Important Note
			- Refer to "references" section of this file to find the documentations for What Is Middleware and JavaScript next Method.
		*/

/*
4. Process a GET request at the "/details" route using postman to retrieve the details of the user.
	Postman
*/

		url: http://localhost:4000/users/details
		method: GET
		headers:
			Type: Bearer Token
			Token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxM2U3N2M5NDIzMTc3YTVhYjlhMDY2ZCIsImVtYWlsIjoiam9obkBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2MzE0ODkyNzV9._8QPUI5S0flEO3pMncEo2bQ_GzTQfzLUM_Z2s8ljAQQ

/*
5. Create a "course.js" file to store the routes for our courses and create a POST route for creating a course.
	Application > routes > course.js
*/

		const express = require("express");
		const router = express.Router();
		const courseController = require("../controllers/course");

		// Route for creating a course
		router.post("/", (req, res) => {
			courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
		})

		// Allows us to export the "router" object that will be accessed in our "index.js" file
		module.exports = router;

/*
6. Implement the main route for our course routes.
	Application > index.js
*/

		/*...*/
		const userRoutes = require("./routes/user");
		const courseRoutes = require("./routes/course");

		const app = express();

		/*...*/
		app.use("/users", userRoutes);
		// Defines the "/courses" string to be included for all course routes defined in the "course" route file
		app.use("/courses", courseRoutes);

		if(require.main === module){
			app.listen(process.env.PORT || 4000, () => {
			    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
			});
		}

		module.exports = app;
/*
7. Create a "course.js" file to store the controller functions that will contain the business logic for our course CRUD operations.
	Application > controllers > course.js
*/

		const Course = require("../models/Course");

		// Create a new course
		/*
			Steps:
			1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
			2. Save the new User to the database
		*/
		module.exports.addCourse = (reqBody) => {

			// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
			// Uses the information from the request body to provide all the necessary information
			let newCourse = new Course({
				name : reqBody.name,
				description : reqBody.description,
				price : reqBody.price
			});

			// Saves the created object to our database
			return newCourse.save().then((course, error) => {

				// Course creation successful
				if (error) {

					return false;

				// Course creation failed
				} else {

					return true;

				};

			});

		};

/*
8. Process a POST request at the "/courses" route using postman to create a new course.
	Postman
*/

		url: http://localhost:4000/courses
		method: POST
		body: raw + JSON
			{
			    "name" : "HTML",
				"description" : "Learn the basics of programming.",
				"price" : 1000
			}

/*
========
Activity
========
*/

/*
1. Refactor the "course" route to implement user authentication for the admin when creating a course.
	Application > routes > course.js
*/

		/*...*/
		const courseController = require("../controllers/course");
		const auth = require("../auth");

		// Route for creating a course
		router.post("/", auth.verify, (req, res) => {
			
			const data = {
				course: req.body,
				isAdmin: auth.decode(req.headers.authorization).isAdmin
			}

			courseController.addCourse(data).then(resultFromController => res.send(resultFromController));

		});

		/*...*/

/*
2. Refactor the "addCourse" controller method to implement admin authentication for creating a course.
	Application > controllers > course.js
*/

		/*...*/

		/*
			Steps:
			1. Create a conditional statement that will check if the user is an administrator.
			2. Create a new Course object using the mongoose model and the information from the request body and the id from the header
			3. Save the new User to the database
		*/
		module.exports.addCourse = (data) => {

			if (data.isAdmin) {

				let newCourse = new Course({
					name : data.course.name,
					description : data.course.description,
					price : data.course.price
				});

				return newCourse.save().then((course, error) => {
					/*...*/
				});

			} else {
				/*...*/
			};
			

		};

/*
===========================================
S35 - Express.js - API Development (Part 4)
===========================================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1_CspcyqQ0SLYMuRBUgFMVgPAjXDs_9NF9X4sOscRYug/edit#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s32-s36

Other References:
	Mongoose Queries
		https://mongoosejs.com/docs/queries.html
	Mongoose find Method
		https://mongoosejs.com/docs/api.html#model_Model.find
	Mongoose findById Method
		https://mongoosejs.com/docs/api.html#model_Model.findById
	Mongoose findByIdAndUpdate Method
		https://mongoosejs.com/docs/api.html#model_Model.findByIdAndUpdate
	Creating Git Projects:
			GitLab
				https://gitlab.com/projects/new#blank_project
			GitHub
				https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
==========
Discussion
========== 
*/

/*
1. Create a route for retrieving all the courses.
	Application > routes > course.js
*/

		/*...*/

		router.post("/", auth.verify, (req, res) => {
			/*...*/
		});

		// Route for retrieving all the courses
		router.get("/all", (req, res) => { //need for middleware

			courseController.getAllCourses().then(resultFromController => res.send(resultFromController));

		});

		module.exports = router;

/*
2. Create a controller method for retrieving all the courses.
	Application > controllers > course.js
*/

		/*...*/

		module.exports.addCourse = (data) => {
			/*...*/
		}

		// Retrieve all courses
		/*
			Steps:
			1. Retrieve all the courses from the database
		*/
		module.exports.getAllCourses = () => {

			return Course.find({}).then(result => {

				return result;

			});

		};

		/*
		Important Note
			- Refer to "references" section of this file to find the documentations for Mongoose Queries and Mongoose find Method.
		*/

/*
3. Process a GET request at the "/all" route using postman to retrieve all the courses.
	Postman
*/

		url: http://localhost:4000/courses/all
		method: GET

/*
4. Create a route for retrieving all the active courses.
	Application > routes > course.js
*/

		/*...*/

		// Route for retrieving all the courses
		router.get("/all", (req, res) => { //need for middleware
			/*...*/
		});

		// Route for retrieving all the ACTIVE courses
		// Middleware for verifying JWT is not required because users who aren't logged in should also be able to view the courses
		router.get("/", (req, res) => {

			courseController.getAllActive().then(resultFromController => res.send(resultFromController));

		});

		module.exports = router;

/*
5. Create a controller method for retrieving all the active courses.
	Application > controllers > course.js
*/

		/*...*/

		module.exports.getAllCourses = () => {
			/*...*/
		};

		// Retrieve all ACTIVE courses
		/*
			Steps:
			1. Retrieve all the courses from the database with the property of "isActive" to true
		*/
		module.exports.getAllActive = () => {

			return Course.find({isActive : true}).then(result => {
				return result;
			});

		};

/*
6. Process a GET request at the "/courses" route using postman to retrieve all the active courses.
	Postman
*/

		url: http://localhost:4000/courses
		method: GET

/*
7. Create a route for retrieving the details of a specific course.
	Application > routes > course.js
*/

		/*...*/

		router.get("/", (req, res) => {
			/*...*/
		});

		// Route for retrieving a specific course
		// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending on the information provided in the url
		router.get("/:courseId", (req, res) => {

			console.log(req.params.courseId);

			// Since the course ID will be sent via the URL, we cannot retrieve it from the request body
			// We can however retrieve the course ID by accessing the request's "params" property which contains all the parameters provided via the url
				// Example: URL - http://localhost:4000/courses/613e926a82198824c8c4ce0e
				// The course Id is "613e926a82198824c8c4ce0e" which is passed via the url that corresponds to the "courseId" in the route
			courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));

		});

		module.exports = router;

/*
8. Create a controller method for retrieving a specific course.
	Application > controllers > course.js
*/

		/*...*/

		module.exports.getAllActive = () => {
			/*...*/
		};

		// Retrieving a specific course
		/*
			Steps:
			1. Retrieve the course that matches the course ID provided from the URL
		*/
		module.exports.getCourse = (reqParams) => {

			return Course.findById(reqParams.courseId).then(result => {
				return result;
			});

		};

		/*
		Important Note
			- Refer to "references" section of this file to find the documentation for Mongoose findById Method.
		*/

/*
9. Process a GET request at the "/courseId" route using postman to retrieve all the active courses.
	Postman
*/

		url: http://localhost:4000/courses/613e926a82198824c8c4ce0e
		method: GET

/*
10. Create a route for updating a course.
	Application > routes > course.js
*/

		/*...*/

		router.get("/:courseId", (req, res) => {
			/*...*/
		});

		// Route for updating a course
		// JWT verification is needed for this route to ensure that a user is logged in before updating a course
		router.put("/:courseId", auth.verify, (req, res) => {

			courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));

		});

		module.exports = router;

/*
11. Create a controller method for updating a course.
	Application > controllers > course.js
*/

		/*...*/

		module.exports.getCourse = (reqParams) => {
			/*...*/
		};

		// Update a course
		/*
			Steps:
			1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
			2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
		*/
		// Information to update a course will be coming from both the URL parameters and the request body
		module.exports.updateCourse = (reqParams, reqBody) => {

			// Specify the fields/properties of the document to be updated
			let updatedCourse = {
				name : reqBody.name,
				description	: reqBody.description,
				price : reqBody.price
			};

			// Syntax
				// findByIdAndUpdate(document ID, updatesToBeApplied)
			return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {

				// Course not updated
				if (error) {

					return false;

				// Course updated successfully
				} else {
					
					return true;
				};

			});

		};

		/*
		Important Note
			- Refer to "references" section of this file to find the documentation for Mongoose findByIdAndUpdate Method.
		*/

/*
12. Process a PUT request at the "/courses" route using postman to create a new course.
	Postman
*/

		url: http://localhost:4000/courses/613e926a82198824c8c4ce0e
		method: POST
		headers:
			Type: Bearer Token
			Token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxM2U3N2M5NDIzMTc3YTVhYjlhMDY2ZCIsImVtYWlsIjoiam9obkBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2MzE0ODkyNzV9._8QPUI5S0flEO3pMncEo2bQ_GzTQfzLUM_Z2s8ljAQQ
		body: raw + JSON
			{
			    "name" : "JavaScript",
				"description" : "Making websites interactive since the 90's.",
				"price" : 2000
			}

/*
========
Activity
========
*/

/*
1. Create a route for archiving a course.
	Application > routes > course.js
*/

		/*...*/

		router.put("/:courseId", auth.verify, (req, res) => {
			/*...*/
		});

		// Route to archiving a course
		// A "PUT" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
		router.put("/:courseId/archive", auth.verify, (req, res) => {

			courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
			
		});

		module.exports = router;

/*
2. Create a controller method for archiving a course.
	Application > controllers > course.js
*/

		/*...*/

		module.exports.updateCourse = (reqParams, reqBody) => {
			/*...*/
		};

		// Archive a course
		// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
		// The "soft delete" happens here by simply updating the course "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active courses are retrieved
		// This allows us access to these records for future use and hides them away from users in our frontend application
		// There are instances where hard deleting records is required to maintain the records and clean our databases
		// The use of "hard delete" refers to removing records from our database permanently
		module.exports.archiveCourse = (reqParams) => {

			let updateActiveField = {
				isActive : false
			};

			return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {

				// Course not archived
				if (error) {

					return false;

				// Course archived successfully
				} else {

					return true;

				}

			});
		};

/*
3. Process a PUT request at the "/courseId/archive" route using postman to archive a course.
	Postman
*/

		url: http://localhost:4000/courses/613e926a82198824c8c4ce0e/archive
		method: POST
		headers:
			Type: Bearer Token
			Token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxM2U3N2M5NDIzMTc3YTVhYjlhMDY2ZCIsImVtYWlsIjoiam9obkBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2MzE0ODkyNzV9._8QPUI5S0flEO3pMncEo2bQ_GzTQfzLUM_Z2s8ljAQQ
		body: raw + JSON
			{
			    "isActive" : false,
			}

/*
===========================================
S36 - Express.js - API Development (Part 5)
===========================================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1IN24Fx5PnrlIq4Bd_UIYwQJg93xqisWRRnH0HuRTrm0/edit#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s32-s36

Other References:
	JavaScript async And await Keywords
		https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Asynchronous/Async_await
	Creating Git Projects:
			GitLab
				https://gitlab.com/projects/new#blank_project
			GitHub
				https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
==========
Discussion
========== 
*/

/*
1. Create a route for enrolling a user.
	Application > routes > user.js
*/

		/*...*/

		router.get("/details", auth.verify, (req, res) => {
			/*...*/
		});

		// Route to enroll user to a course
		router.post("/enroll", (req, res) => {

			let data = {
				userId : req.body.userId,
				courseId : req.body.courseId
			}

			userController.enroll(data).then(resultFromController => res.send(resultFromController));

		})

		module.exports = router;

/*
2. Create a controller method for enrolling the user to a specific course.
	Application > controllers > user.js
*/

		const User = require("../models/User");
		const Course = require("../models/Course");
		const bcrypt = require("bcrypt");
		/*...*/

		module.exports.getProfile = (data) => {
			/*...*/
		};

		// Enroll user to a class
		/*
			Steps:
			1. Find the document in the database using the user's ID
			2. Add the course ID to the user's enrollment array
			3. Update the document in the MongoDB Atlas Database
		*/
		// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user
		module.exports.enroll = async (data) => {

			// Add the course ID in the enrollments array of the user
			// Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
			// Using the "await" keyword will allow the enroll method to complete updating the user before returning a response back to the frontend
			let isUserUpdated = await User.findById(data.userId).then(user => {

				// Adds the courseId in the user's enrollments array
				user.enrollments.push({courseId : data.courseId});

				// Saves the updated user information in the database
				return user.save().then((user, error) => {
					if (error) {
						return false;
					} else {
						return true;
					};
				});

			});

			// Add the user ID in the enrollees array of the course
			// Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend
			let isCourseUpdated = await Course.findById(data.courseId).then(course => {

				// Adds the userId in the course's enrollees array
				course.enrollees.push({userId : data.userId});

				// Saves the updated course information in the database
				return course.save().then((course, error) => {
					if (error) {
						return false;
					} else {
						return true;
					};
				});

			});

			// Condition that will check if the user and course documents have been updated
			// User enrollment successful
			if(isUserUpdated && isCourseUpdated){
				return true;
			// User enrollment failure
			} else {
				return false;
			};

		};

		/*
		Important Note
			- Refer to "references" section of this file to find the documentation for JavaScript async And await Keywords.
		*/

/*
3. Process a POST request at the "/enroll" route using postman to enroll a user to a course.
	Postman
*/

		url: http://localhost:4000/users/enroll
		method: POST
		body: raw + JSON
			{
			    "userId" : "613e77c9423177a5ab9a066d",
				"courseId" : "613e926a82198824c8c4ce0e"
			}

/*
========
Activity
========
*/

/*
1. Refactor the "user" route to implement user authentication for the "enroll" route.
	Application > routes > course.js
*/

		/*...*/

		router.get("/details", auth.verify, (req, res) => {
			/*...*/
		});

		router.post("/enroll", auth.verify, (req, res) => {

			let data = {
				// User ID will be retrieved from the request header
				userId : auth.decode(req.headers.authorization).id,
				// Course ID will be retrieved from the request body
				courseId : req.body.courseId
			}

			userController.enroll(data).then(resultFromController => res.send(resultFromController));

		});

		/*...*/

/*
2. Process a POST request at the "/enroll" route using postman to enroll a user to a course.
	Postman
*/

		url: http://localhost:4000/users/enroll
		method: POST
		headers:
			Type: Bearer Token
			Token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxM2U3N2M5NDIzMTc3YTVhYjlhMDY2ZCIsImVtYWlsIjoiam9obkBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2MzE0ODkyNzV9._8QPUI5S0flEO3pMncEo2bQ_GzTQfzLUM_Z2s8ljAQQ
		body: raw + JSON
			{
				"courseId" : "613e926a82198824c8c4ce0e"
			}