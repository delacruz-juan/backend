# WDC028 - S35 - Express.js - API Development (Part 4)

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1_CspcyqQ0SLYMuRBUgFMVgPAjXDs_9NF9X4sOscRYug/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1_CspcyqQ0SLYMuRBUgFMVgPAjXDs_9NF9X4sOscRYug/edit#slide=id.gaaf8d9a761_0_609) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s32-s36) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/backend/s32-s36/Manual.js) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                             | Link                                                         |
| --------------------------------- | ------------------------------------------------------------ |
| Mongoose Queries                  | [Link](https://mongoosejs.com/docs/queries.html)             |
| Mongoose find Method              | [Link](https://mongoosejs.com/docs/api.html#model_Model.find) |
| Mongoose findById Method          | [Link](https://mongoosejs.com/docs/api.html#model_Model.findById) |
| Mongoose findByIdAndUpdate Method | [Link](https://mongoosejs.com/docs/api.html#model_Model.findByIdAndUpdate) |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[1 hr 30 mins] - Create Routes
		- /all
		- /courses
		- /courseId
		- /courseId/Archive
	2.[1 hr 30 mins] - Create Controllers
		- getAllCourses()
		- getAllActive()
		- getCourse()
		- updateCourse()
		- archiveCourse()
	3.[1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

​	

[Back to top](#table-of-contents)

