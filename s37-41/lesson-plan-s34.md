# WDC028 - S34 - Express.js - API Development (Part 3)

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1IN24Fx5PnrlIq4Bd_UIYwQJg93xqisWRRnH0HuRTrm0/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1IN24Fx5PnrlIq4Bd_UIYwQJg93xqisWRRnH0HuRTrm0/edit#slide=id.gca1de815d2_0_0) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s32-s36) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/backend/s32-s36/Manual.js) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                      | Link                                                         |
| -------------------------- | ------------------------------------------------------------ |
| JSON Web Tokens            | [Link](https://jwt.io/)                                      |
| jsonwebtoken Package       | [Link](https://www.npmjs.com/package/jsonwebtoken)           |
| What Is Middleware         | [Link](https://www.redhat.com/en/topics/middleware/what-is-middleware) |
| JavaScript next Method     | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Generator/next) |
| JavaScript typeof Operator | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof) |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[1 hr] - Auth/JWT Methods
		- verify()
		- decode()
	2.[1 hr] - Create/modify Routes
		- /details
		- /courses
	3.[1 hr] - Create Controllers
		- addCourse()
	4.[1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

​	

[Back to top](#table-of-contents)